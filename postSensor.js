var mongo = require('./mongo.js')

// Authorization test.
function authorisationSent(auth) {
	console.log('Checking authentication.')
	return new Promise((resolve, reject) => {
		console.log('  a')
		if (auth.scheme !== 'Basic') {
			console.log('  b')
			console.log('Basic authentication missing.')
			return reject({code: 401, response:{ status:'error', message:'Basic access authentication required.'}})
		}
		if (auth.basic.username === 'davids' || auth.basic.password === 'schillid') {
			console.log('  c')
			console.log('Access granted. Correct username and password.')
			return resolve({code: 200, response:{ status:'success', message:'Access granted.'}})
		}
		console.log('  d')
		return reject({code: 401, response:{ status:'error', message:'Access denied. Wrong username and/or password.'}})
	})
}

// Testing if all data was provided.
function checkBody(body) {
	console.log('Checking body.')
	return new Promise((resolve, reject) => {
		console.log('  a')
		console.log(body)
		if (body == undefined || typeof body.owner !== 'string' || typeof body.name !== 'string' || typeof body.reading !== 'string' || typeof body.type !== 'string') {
			console.log('  b')
			return reject({code: 400, response:{status:'error', message:'Owner, sensor name, type and reading are necessary.'}})
		}
		console.log('Provided data accepted.')
		return resolve({code: 200, response:{status:'success', message:'Owner, sensor name, type and reading present.'}})
	})
}

// Testing if the sensor already exists.
function checkExisting(sensor) {
	console.log('Checking if sensor '+sensor.name+' already exists in database for user '+sensor.owner+'.')
	return new Promise((resolve, reject) => {
		console.log('  a')
		mongo.nameExists(sensor.name, response => {
			console.log(response)
			if (response === true) {
				console.log('Sensor already in database. Sensor not added.')
				return reject({code: 400, response:{status: 'error', message:'This sensor already exists. Sensor not added.'}})
			}
			return resolve({code: 200, response:{status:'success', message:'Data stored to database.'}})
		})
	})
}

function checkBodyPi(body) {
	console.log('Checking body.')
	return new Promise((resolve, reject) => {
		console.log('  a')
		console.log(body)
		
		const piDataArray = body.split('&')
		const owner = piDataArray[0].substring(piDataArray[0].indexOf('=') + 1)
		const name = piDataArray[1].substring(piDataArray[1].indexOf('=') + 1)
		const reading = piDataArray[2].substring(piDataArray[2].indexOf('=') + 1)
		const type = piDataArray[3].substring(piDataArray[3].indexOf('=') + 1)
		
		const piData = {owner: owner, name: name, reading: reading, type: type}
		
		if (piData.name === undefined || typeof piData.owner !== 'string' || typeof piData.name !== 'string' || typeof piData.reading !== 'string' || typeof piData.type !== 'string') {
			console.log('  b')
			return reject({code: 400, response:{status:'error', message:'Owner, sensor name, type and reading are necessary.'}})
		}
		console.log('Provided data accepted.')
		return resolve({code: 200, response:{status:'success', message:'Owner, sensor name, type and reading present.'}, data: piData})
	})
}

/* Running the Mongo module in order to store the object to the database. */
function storeSensor(sensor) {
	console.log('Storing sensor to database.')
	return new Promise((resolve, reject) => {
		console.log('  a')
		mongo.addSensor(sensor, response => {
			console.log(response)
			return resolve({code: 200, response:{status:'success', message:'Data stored to database.', data:response}})
		})
	})
}

// Broadcaster ADD main promise chain. Branching off for a Raspberry Promise chain or the default one for form-data.
exports.addSensor = (auth, body, device, callback) => {
	console.log('---1---')
	if (device === 'RaspberryPi'){
		console.log('Data received from RaspberryPi. Establishing alternate Promise chain.')
		var piData
		authorisationSent(auth)
		.then(() => {
			console.log('---2---')
			return checkBodyPi(body)
		}).then((output) => {
			console.log('---3---')
			piData = output.data
			return checkExisting(output.data)
		}).then(() => {
			console.log('---4---')
			return storeSensor(piData)
		}).then(() => {
			console.log('---5---')
			callback({code:201, response:{status:'success', message:'Sensor added.', data: piData}})
		}).catch((data) => {
			console.log('---ERROR---')
			console.log('MAIN CATCH')
			callback(data)
		})
	} else {
		console.log('Data not from Raspberry, assuming form-data for default Promise chain.')
		authorisationSent(auth)
		.then(() => {
			console.log('---2---')
			return checkBody(body)
		}).then(() => {
			console.log('---3---')
			return checkExisting(body)
		}).then(() => {
			console.log('---4---')
			return storeSensor(body)
		}).then((data) => {
			console.log('---5---')
			callback({code:201, response:{status:'success', message:'Sensor added.', data: data.response.data}})
		}).catch((data) => {
			console.log('---ERROR---')
			console.log('MAIN CATCH')
			callback(data)
		})
	}
}