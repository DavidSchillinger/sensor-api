var mongoose = require('mongoose')
var uuid = require('node-uuid')
/* the database name is stored in a private variable instead of being 'hard-coded' so it can be replaced using the 'rewire' module. This avoids the need for the unit tests to run against the 'live' database. */
var database = 'api'
/* the server connections string includes both the database server IP address and the name of the database. */
const server = 'mongodb://'+process.env.IP+'/'+database
console.log(server)
/* the mongoose drivers connect to the server and return a connection object. */
mongoose.connect(server)
const db = mongoose.connection

/* all documents in a 'collection' must adhere to a defined 'schema'. Here we define a new schema that includes a mandatory string and an array of strings. */
const sensorsSchema =
new mongoose.Schema({
	owner: String,
	sensor: {
		name: { type: String },
		reading: { type: String },
		type: { type: String },
		id: { type: String }
	},
})
/* the schema is associated with the 'List' collection which means it will be applied to all documents added to the collection. */
const Sensors = mongoose.model('Sensors', sensorsSchema)
/* END OF MONGOOSE SETUP */

/* notice we are using the 'arrow function' syntax. In this example there are more than one parameter so they need to be enclosed in brackets. */
exports.addSensor = (input, callback) => {
	console.log('Adding sensor...')
	var newSensors = new Sensors
	newSensors.owner = input.owner
	newSensors.sensor.id = uuid.v4().toUpperCase()
	newSensors.sensor.type = input.type
	newSensors.sensor.reading = input.reading
	newSensors.sensor.name = input.name
	
	newSensors.save((err, data) => {
		if (err) {
			callback('error: '+err)
		}
		var response = {
			owner: data.owner,
			sensor: {
				name: data.sensor.name,
				reading: data.sensor.reading,
				type: data.sensor.type,
				id: data.sensor.id
			}
		}
		callback(response)
	})
}

exports.updateSensor = (input, callback) => {
	const owner = input.owner
	const name = input.name
	const reading = input.reading
	
	const update = {$set: {'sensor.reading': reading}}
	
	Sensors.update({owner: owner, 'sensor.name': name}, update, (err, data) => {
		if (err) {
			callback('error: '+err)
		}
		callback(data)
	})
}

exports.testID = (input, callback) => {
	const owner = input.owner
	const name = input.name
	const id = input.id
	
	Sensors.find({owner: owner, 'sensor.name': name, 'sensor.id': id}, (err, data) => {
		if (err) {
			callback('error: '+err)
		} else if (data[0] != undefined) {
			callback({code: 200, response: {owner: data[0].owner, sensor: data[0].sensor}})
		} else {
			callback({code: 404, response: {message: 'No sensor with name: '+name+' and ID: '+id+' found.'}})
		}
	})
}

exports.getSensors = (sensorName, sensorType, owner, callback) => {
  // the List object contains several useful properties. The 'find' property contains a function to search the MongoDB document collection.
	if (sensorName != undefined) {
		Sensors.find({'sensor.name': sensorName, owner: owner}, (err, data) => {
			if (err) {
				callback('error: '+err)
			} else if (data[0] != undefined) {
				callback({code: 200, response: {owner: data[0].owner, sensor: {name: data[0].sensor.name, reading: data[0].sensor.reading, type: data[0].sensor.type, id: data[0].sensor.id}}})
			}  else {
				callback({code: 404, response: {message: 'No sensor with name: '+sensorName+' found.'}})
			}
		})
	} else if (sensorType != undefined) {
		Sensors.find({'sensor.type': sensorType, owner: owner}, (err, data) => {
			if (err) {
				callback('error: '+err)
			} else if (data[0] != undefined) {
				const list = data.map(item => {
					return {name: item.sensor.name, reading: item.sensor.reading, type: item.sensor.type, id: item.sensor.id}
				})
				callback({code: 200, response: {owner: owner, type: sensorType, sensors: list}})
			} else {
				callback({code: 404, response: {message: 'No sensor of type: '+sensorType+' found.'}})
			}
		})
	} else if (owner !== undefined) {
		Sensors.find({owner: owner}, (err, data) => {
			if (err) {
				callback('error: '+err)
			} else if (data[0] != undefined) {
				const list = data.map(item => {
					return {name: item.sensor.name, reading: item.sensor.reading, type: item.sensor.type, id: item.sensor.id}
				})
				callback({code: 200, response: {owner: owner, type: sensorType, sensors: list}})
			} else {
				callback({code: 404, response: {message: 'No sensor for owner: '+owner+' found.'}})
			}
		})
	} else {
		callback({code: 404, response: {message: 'At least an owner has to be specified to lookup a sensor.'}})
	}
}

/*exports.getById = (id, callback) => {
  // the 'find' property function can take a second 'filter' parameter.
  Sensors.find({_id: id}, (err, data) => {
	if (err) {
	  callback('error: '+err)
	}
	callback(data)
  })
}*/

exports.deleteSensor = (input, callback) => {
	Sensors.remove({'sensor.name': input.name, owner: input.owner}, (err, data) => {
		if (err) {
			callback('error: '+err)
		}
		callback({code: 200, response: {message: 'Sensor '+input.name+' successfully removed from database.'}})
	})
}

/*exports.clear = (callback) => {
	Sensors.remove({}, err => {
		if (err) {
			callback('error: '+err)
		}
		callback('Database emptied.')
	})
}*/

exports.nameExists = (name, callback) => {
	Sensors.findOne({'sensor.name': name}, (err, data) => {
		if (err) {
			console.log('error: '+err)
			callback(true)
		} else if (data == null) {
			callback(false)
		} else {
			callback(true)
		}
	})
}