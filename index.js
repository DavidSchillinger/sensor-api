var restify = require('restify')
var server = restify.createServer()

server.use(restify.fullResponse())
server.use(restify.queryParser())
server.use(restify.bodyParser())
server.use(restify.authorizationParser())

restify.CORS.ALLOW_HEADERS.push('authorization')
restify.CORS.ALLOW_HEADERS.push('owner')

var postSensor = require('./postSensor.js')
var getSensor = require('./getSensor.js')
var putSensor = require('./putSensor.js')
var delSensor = require('./delSensor.js')

var port = process.env.PORT || 8080
server.listen(port, (err) => {
	if (err) {
		console.error(err)
	} else {
		console.log('API is ready at: ' + port)
	}
})

server.get('/sensors', (req, res) => {
	console.log('GET from /sensors.')
	const auth = req.authorization
	const sensorName = req.query.name
	const sensorType = req.query.type
	const contentType = req['headers']['content-type']
	const owner = req.headers.owner
	getSensor.searchSensors(auth, owner, sensorName, sensorType, contentType, (data) => {
		console.log('DATA RETURNED')
		console.log(data)
		res.send(data.code, data.response)
		res.end()
	})
})

server.post('/sensors', (req, res) => {
	console.log('POST to /sensors.')
	const auth = req.authorization
	const body = req.body
	const device = req.headers.device
	postSensor.addSensor(auth, body, device, (data) => {
		console.log('DATA RETURNED')
		console.log(data)
		res.setHeader('content-type', 'application/json')
		res.send(data.code, data.response)
		res.end()
	})
})

server.put('/sensors', (req, res) => {
	console.log('PUT to /sensors.')
	const auth = req.authorization
	const body = req.body
	putSensor.updateSensor(auth, body, (data) => {
		console.log('DATA RETURNED')
		console.log(data)
		res.setHeader('content-type', 'application/json')
		res.send(data.code, data.response)
		res.end()
	})
})

server.del('/sensors', (req, res) => {
	console.log('DELETE from /sensors.')
	const auth = req.authorization
	const body = req.body
	delSensor.removeSensor(auth, body, (data) => {
		console.log('DATA RETURNED')
		console.log(data)
		res.setHeader('content-type', 'application/json')
		res.send(data.code, data.response)
		res.end()
	})
})