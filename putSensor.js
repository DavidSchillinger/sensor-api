var mongo = require('./mongo.js')

// Authorization test.
function authorisationSent(auth) {
	console.log('Checking authentication.')
	return new Promise((resolve, reject) => {
		console.log('  a')
		if (auth.scheme !== 'Basic') {
			console.log('  b')
			console.log('Basic authentication missing.')
			return reject({code: 401, response:{ status:'error', message:'Basic access authentication required.'} })
		}
		if (auth.basic.username === 'davids' || auth.basic.password === 'schillid') {
			console.log('  c')
			console.log('Access granted. Correct username and password.')
			return resolve({code: 200, response:{ status:'success', message:'Access granted.'} })
		}
		console.log('  d')
		return reject({code: 401, response:{ status:'error', message:'Access denied. Wrong username and/or password.'} })
	})
}

// Testing if all data was provided.
function checkBody(body) {
	console.log('Checking body.')
	return new Promise((resolve, reject) => {
		console.log('  a')
		console.log(body)
		if (body == undefined || typeof body.owner !== 'string' || typeof body.name !== 'string' || typeof body.reading !== 'string') {
			console.log('  b')
			return reject({code: 400, response:{status:'error', message:'Owner, sensor name, type and reading are necessary.'}})
		}
		console.log('Provided data accepted, testing ID.')
		mongo.testID(body, response => {
			if (response.code === 404){
				return reject({code: 400, response:{status:'error', message:'Sensor not found.'}})
			}
			return resolve({code: 200, response:{status:'success', message:'Owner, sensor name, type and reading present.'}})
		})
	})
}

// Running the Mongo module in order to update the database entry.
function update(input) {
	console.log('Updating Sensor entry.')
	return new Promise((resolve, reject) => {
		console.log('  a')
		mongo.updateSensor(input, response => {
			console.log(response)
			return resolve({code: 200, response:{status:'success', message:'Sensor entry updated.', data: response}})
		})
	})
}

// Running the Mongo module in order to update the database entry.
function retrieve(input) {
	console.log('Returning updated Sensor entry.')
	return new Promise((resolve, reject) => {
		console.log('  a')
		mongo.getSensors(input.name, undefined, input.owner, data => {
			console.log(data)
			return resolve(data)
		})
	})
}

// Sensor PUT main promise chain.
exports.updateSensor = (auth, body, callback) => {
	console.log('---1---')
	authorisationSent(auth)
	.then(() => {
		console.log('---2---')
		return checkBody(body)
	}).then(() => {
		console.log('---3---')
		return update(body)
	}).then(() => {
		console.log('---4---')
		return retrieve(body)
	}).then((data) => {
		console.log('---5---')
		callback({code:200, response:{status:'success', message:'Sensor updated.', data: data.response}})
	}).catch((data) => {
		console.log('---ERROR---')
		console.log('MAIN CATCH')
		callback(data)
	})
}