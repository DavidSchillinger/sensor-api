var mongo = require('./mongo.js')

// Authorization test.
function authorisationSent(auth) {
	console.log('Checking authentication.')
	return new Promise((resolve, reject) => {
		console.log('  a')
		if (auth.scheme !== 'Basic') {
			console.log('  b')
			console.log('Basic authentication missing.')
			return reject({code: 401, response:{ status:'error', message:'Basic access authentication required.'}})
		}
		if (auth.basic.username === 'davids' || auth.basic.password === 'schillid') {
			console.log('  c')
			console.log('Access granted. Correct username and password.')
			return resolve({code: 200, response:{ status:'success', message:'Access granted.'}})
		}
		console.log('  d')
		return reject({code: 401, response:{ status:'error', message:'Access denied. Wrong username and/or password.'}})
	})
}

// Testing if all data was provided.
function checkBody(body) {
	console.log('Checking body.')
	return new Promise((resolve, reject) => {
		console.log('  a')
		console.log(body)
		if (body == undefined || typeof body.owner !== 'string' || typeof body.name !== 'string') {
			console.log('  b')
			return reject({code: 400, response:{status:'error', message:'Owner and sensor name are necessary.'}})
		}
		console.log('Provided data accepted.')
		return resolve({code: 200, response:{status:'success', message:'Owner and sensor name present.'}})
	})
}

// Running the Mongo module in order to remove the database entry.
function remove(body) {
	console.log('Removing sensor from database.')
	return new Promise((resolve, reject) => {
		console.log('  a')
		mongo.deleteSensor(body, data => {
			console.log(data)
			return resolve({code: 200, response:{status:'success', message:'Sensor successfully removed from database.'}})
		})
	})
}

exports.removeSensor = (auth, body, callback) => {
	console.log('---1---')
	authorisationSent(auth)
	.then(() => {
		console.log('---2---')
		return checkBody(body)
	}).then(() => {
		console.log('---3---')
		return remove(body)
	}).then(() => {
		console.log('---4---')
		callback({code:200, response:{status:'success', message:'Sensor removed.', data: body}})
	}).catch((data) => {
		console.log('---ERROR---')
		console.log('MAIN CATCH')
		callback(data)
	})
}