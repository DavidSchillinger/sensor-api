var mongo = require('./mongo.js')
var xml2js = require('xml2js')

// Authorization test.
function authorisationSent(auth) {
	console.log('Checking authentication.')
	return new Promise((resolve, reject) => {
		console.log('  a')
		if (auth.scheme !== 'Basic') {
			console.log('  b')
			console.log('Basic authentication missing.')
			return reject({code: 401, response:{ status:'error', message:'Basic access authentication required.'}})
		}
		if (auth.basic.username === 'davids' || auth.basic.password === 'schillid') {
			console.log('  c')
			console.log('Access granted. Correct username and password.')
			return resolve({code: 200, response:{ status:'success', message:'Access granted.'}})
		}
		console.log('  d')
		return reject({code: 401, response:{ status:'error', message:'Access denied. Wrong username and/or password.'}})
	})
}

function search(sensorName, sensorType, owner, contentType) {
	console.log('Looking up sensor(s).')
	return new Promise((resolve, reject) => {
		console.log('  a')
		mongo.getSensors(sensorName, sensorType, owner, info => {
			var data = info
			if (contentType === 'application/json') {
				console.log('application/json specified. No conversion required.')
				data.contentType = 'application/json'
				return resolve(data)
			} else if (contentType === 'application/xml' ) {
				console.log('application/xml specified. Converting data to XML.')
				data.contentType = 'application/xml'
				const jsonTemp = JSON.stringify(data.response.data)
				console.log(jsonTemp)
				const json = JSON.parse(jsonTemp)
				var builder = new xml2js.Builder()
				var newXML = builder.buildObject(json)
				data.response = newXML
				return resolve(data)
			} else {
				console.log('No content-type specified. Default JSON output.')
				data.contentType = 'application/json'
				return resolve(data)
			}
		})
	})
}

exports.searchSensors = (auth, owner, sensorName, sensorType, contentType, callback) => {
	var sensors
	console.log('---1---')
	authorisationSent(auth)
	.then(() => {
		console.log('---2---')
		return search(sensorName, sensorType, owner, contentType)
	}).then((data) => {
		console.log('---3---')
		sensors = data
	}).then(() => {
		console.log('---4---')
		callback(sensors)
	}).catch((data) => {
		console.log('---ERROR---')
		console.log('MAIN CATCH')
		callback(data)
	})
}